var gulp         = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var babel        = require('gulp-babel');
var browserSync  = require('browser-sync');
var concat       = require('gulp-concat');
var eslint       = require('gulp-eslint');
var filter       = require('gulp-filter');
var newer        = require('gulp-newer');
var notify       = require('gulp-notify');
var plumber      = require('gulp-plumber');
var reload       = browserSync.reload;
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');

var onError = function(err) {
    notify.onError({
        title:    "Error",
        message:  "<%= error %>",
    })(err);
    this.emit('end');
};

var plumberOptions = {
    errorHandler: onError,
};

var jsFiles = {
    vendor: [

    ],
    source: [
        'assets/jsx/create-store.jsx',
        'assets/jsx/date-picker.jsx',
        'assets/jsx/time-zone.jsx',
        'assets/jsx/format-dates.jsx',
        'assets/jsx/text-time.jsx',
        'assets/jsx/widget.jsx',
        'assets/jsx/ComponentForm.jsx'

    ]
};

// Lint JS/JSX files
gulp.task('eslint', function() {
    return gulp.src(jsFiles.source)
        .pipe(eslint({
            baseConfig: {
                "ecmaFeatures": {
                    "jsx": true
                }
            }
        }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// Copy react.js and react-dom.js to assets/js/src/vendor
// only if the copy in node_modules is "newer"
gulp.task('copy-react', function() {
    return gulp.src('node_modules/react/dist/react.js')
        .pipe(newer('assets/js/src/vendor/react.js'))
        .pipe(gulp.dest('assets/js/src/vendor'));
});
gulp.task('copy-react-dom', function() {
    return gulp.src('node_modules/react-dom/dist/react-dom.js')
        .pipe(newer('assets/js/src/vendor/react-dom.js'))
        .pipe(gulp.dest('assets/js/src/vendor'));
});
gulp.task('copy-redux', function() {
    return gulp.src('node_modules/redux/dist/redux.js')
        .pipe(newer('assets/js/src/vendor/redux.js'))
        .pipe(gulp.dest('assets/js/src/vendor'));
});
gulp.task('copy-react-redux', function() {
    return gulp.src('node_modules/react-redux/dist/react-redux.js')
        .pipe(newer('assets/js/src/vendor/react-redux.js'))
        .pipe(gulp.dest('assets/js/src/vendor'));
});
// Copy assets/js/vendor/* to assets/js
gulp.task('copy-js-vendor', function() {
    return gulp
        .src([
            'assets/js/src/vendor/react.js',
            'assets/js/src/vendor/react-dom.js',
            'assets/js/src/vendor/redux.js',
            'assets/js/src/vendor/react-redux.js',
        ])
        .pipe(gulp.dest('assets/js'));
});

// Concatenate jsFiles.vendor and jsFiles.source into one JS file.
// Run copy-react and eslint before concatenating
gulp.task('concat', ['copy-react', 'copy-react-dom', 'copy-redux', 'copy-react-redux'], function() {
    return gulp.src(jsFiles.vendor.concat(jsFiles.source))
        .pipe(sourcemaps.init())
        .pipe(babel({
            only: [
                'assets/jsx',
            ],
            compact: false
        }))
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/js'));
});

// Compile Sass to CSS
gulp.task('sass', function() {
    var autoprefixerOptions = {
        browsers: ['last 2 versions'],
    };

    var filterOptions = '**/*.css';

    var reloadOptions = {
        stream: true,
    };

    var sassOptions = {
        includePaths: [

        ]
    };

    return gulp.src('assets/sass/**/*.scss')
        .pipe(plumber(plumberOptions))
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css'))
        .pipe(filter(filterOptions))
        .pipe(reload(reloadOptions));
});

// Watch JS/JSX and Sass files
gulp.task('watch', function() {
    gulp.watch('assets/jsx/**/*.{js,jsx}', ['concat']);
    gulp.watch('assets/sass/**/*.scss', ['sass']);
});

// BrowserSync
gulp.task('browsersync', function() {
    browserSync({
        server: {
            baseDir: './'
        },
        open: false,
        online: false,
        notify: false,
    });
});

gulp.task('build', ['sass', 'copy-js-vendor', 'concat']);
gulp.task('default', ['build', 'browsersync', 'watch']);
/*
'use strict';
const gulp          = require('gulp');
const sass          = require('gulp-sass');
const sourcemaps    = require('gulp-sourcemaps'); //создает mapa
const debug         = require('gulp-debug'); // можно настроить и запускать при этом будет видно что и когда запускается и как выполняется
const del           = require('del');
const newer         = require('gulp-newer'); // получает конечный путь и если в нем есть файл который не менялся или с новой датой то он его не перезаписывает
const gulpIf        = require('gulp-if');
/!*  gulp-if это директива работает как if else ниже мы ее используем для
 *  2-х сборок одна как для продакшени без mapa
 *  вторая с mapa в нутри css для разработки
 *  для продакшена запускать как !!!!!!!!!!!!!!!!!!!!!!
 *  NODE_ENV = production gulp style
*!/
const remember     = require('gulp-remember');// запоминает пропущеные через себя файлы и добавляет их при зборке
const cached       = require('gulp-cached');  // запоминает файл и в каком он состоянии если не изменялся файл он его не пропускает
const path         = require('path'); // отдает полные путь к файлу
const autoprefixer = require('gulp-autoprefixer'); // добавляет авто префиксы
const minifyCss    = require('gulp-minify-css');
const browserSync  = require('browser-sync');
const notify       = require('gulp-notify');
const plumber      = require('gulp-plumber'); // добавляет к потокам свою метку пайпинга и если будет ошибка он вызовет нашу функцию

/!* gulp.src('js/!**\/!*.js')
* .pipe (debug({title: 'src'}))
* .pipe(sass())
* .pipe (debug({title: 'sass'}))
*
* вот так пользоватся дебагом в  title - мы описываем
* после какой операции пришел файл
*
* *!/
gulp.task('prefixer', function () {
    return gulp.src('public/!**!/!*.css')
        .pipe(autoprefixer())
        .pipe(minifyCss())
        .pipe(gulp.dest('public'))
});


const isDevelopment = !process.env.NODE_ENV ||  process.env.NODE_ENV == 'development';

gulp.task('styles', function () {
return gulp.src('sass/!**!/style.scss')
    .pipe(plumber({
        errorHandler: notify.onError(function (err) {
            return{
                message: err.message
            };
        })
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(cached('styles')) // запоминает файлы и не пропускает старые файлы для обработки ныже в  (task "watch" ) мы прописали для него забыть то что удалили
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(remember('styles')) // кеш который запоминает файлы пропущенные через него в первый раз и добавляет те которые отбросил фильтер
    .pipe(minifyCss())
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('public/css'))
});


/!*
* clean  - этот таск используется для удаления директории указаной в нем
*
* *!/
gulp.task('clean', function () {
    return del('public');
});

gulp.task('test', function () {
    return gulp.src('js/!**!/!*.*', {since: gulp.lastRun('test')}) // {since: gulp.lastRun('test')} - это приписка значит что мы берем и меняем только те файлы
        .pipe(newer('public/js'))
        .pipe(gulp.dest('public/js'));                          // ДАТА ИЗМЕНЕНИЯ которых отличается от первой сборки этого таска ИЛИ НОВЫЕ ФАЙЛЫ
});
/!*
* bild - используется для запуска группы тасков на выполнение в НЕМ ТАК ЖЕ ЕСТЬ ПАРАЛЕЛЬНО ВЫПОЛНЯЕМЫЕ ЗАДАЧИ
*
* *!/
gulp.task('build', gulp.series('clean', gulp.parallel('styles')));

/!*
* создание вотчера который будет пересобирать проект при изменении файлов, путь к которым мы укажем
 *  !!!! ГЛАВНОЕ gulp.watch СЛЕДИТ ЗА ФАЙЛАМИ ПРИ ИЗМЕНЕНИИ ФАЙЛОВ ЗАПУСКАЕТ ЗАДАЧУ task
 *  ЕЁ НУЖНО ОБОРАЧИВАТЬ В gulp.series('test') ДАЖЕ ЕСЛИ ЗАДАЧА ОДНА В ДАННОМ СЛУЧАИ ('test')!!!!!!!!
 * *!/
gulp.task('watch', function () {
    gulp.watch('sass/!**!/!*.*', gulp.series('styles')).on('unlink', function (filepath) {
  console.log(path.basename('sass/style.scss'));
        remember.forget('styles', path.basename(filepath));
        delete cached.caches.styles[path.basename(filepath)]; //заставляем забыть удаленные файл. Если мы его вернем то он станет как бы новым
    }); // наблюдаем за всеми изменениями в данной директории и если что то изменится пересобирем файлы
       //!!! on повесили обработчик на событие удаление файлов и просим забыть (remember) файл который был удален!!!!
       //  !!!!! это нужно если мы собираем все файлы в один БЕЗ ИСПОЛЬЗОВАНЯИ @IMPORT в главном файле
       //   !!!!!то если один за одним с путем sass/!**!/!*.*

   /!* gulp.watch('js/!**!/!*.*', gulp.series('test'));*!/
});

/!* одна задача которая запускает сборку проекта а потом вотчеры
* *!/
gulp.task('dev', gulp.series('build', 'watch'));/!*если нужно запусить watch и server то это делается gulp.parallel('watch', 'server')!!! *!/

gulp.task('server',function(){
    browserSync.init({
        server:'./' // директория запуска сервера можноуказать папку с продакшеном
    });
    browserSync.watch('./!**!/!*.*').on('change', browserSync.reload); // создали вотчера который следит за ВСЕМИ ФАЙЛАМИ В ДЕРЕКТОРИИ И ПРИ ИЗМЕНЕНИИ ДОБАЛЯЕТ НА СТРАНИЦУ
});



gulp.task('default', function () {
    return gulp.src('js/!**!/!*.{js,css}')
        .on('data', function (file) {
            console.log({
                content: file.content,
                path: file.path,
                base: file.base,
                /// path helper
                relative: file.relative,
                dirname: file.dirname,
                basename: file.basename,
                extname: file.extname
            })
        })
        .pipe(gulp.dest('./testdest/'))
});


*/



















/*
gulp.task('sass:watch', function () {
    gulp.watch('./sass/!**!/!*.scss', ['sass']);
});

gulp.task('minCss',function () {
    return gulp.src('css/!**!/!*.css')
        .pipe(minifyCss({KeepBreak:true}))
        .pipe(gulp.dest('app/css/'))
});

gulp.task('minJson', function () {
    gulp.src('./!**!/!*.json')
        .pipe(jsonmin())
        .pipe(gulp.dest('./app/'));
});*/
