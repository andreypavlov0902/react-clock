class TimeZone extends React.Component {
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
    }
    options(arr) {
        let res = [];
        arr.forEach((element, i) => {
            res.push(<option key={i} value={element}>{element}</option>)
        });
        return res
    }
    change() {
        store.dispatch({type: 'CHANGE_TIME_ZONE', value: this.select.value});
        console.log('select', this.select.value)
    }

    render(){
        if(typeof moment.tz === 'function') {
            return (
                <div className="time-zone form-group my-form-group col-md-3">
                    <label className=" my-label" htmlFor="zone">Choice the time zone</label>
                    <select className="custom-select " onChange={this.change} size="1" name="timeZone" id="zone" ref={(select) => { this.select = select; }}>
                        {this.options(timeZone)}
                    </select>

                </div>
            )
        }
        return (
            <div>
               <pre>if you connect moment-timezone.js
                    library instead of moment.js
                    to you will be available time zone choice
               </pre>
            </div>
        )
    }
}