class DatePicker extends React.Component{
    constructor(props) {
        super(props);
        this.changeDate1 = this.changeDate1.bind(this);
        this.id = DatePicker.id++;
    }
    render() {
        return (
            <div className="form-group custom my-form-group col-md-3">
                <label className="example-datetime-local-input" htmlFor={'datetimepicker' + this.id}>Enter Date</label>
                <input className="form-control md" onChange={this.changeDate1} id={'datetimepicker' + this.id} type="text" ref={(input) => { this.trackInput = input; }}/>

            </div>

        )
    }
    componentDidMount(){
        let select =`#datetimepicker${this.id}`;
        $(select).datetimepicker({
            onSelectDate: (ct,$i) => {
                store.dispatch({type: 'CHANGE_INPUT', value: ct});
            },
            onSelectTime: (ct, $input) =>{
                store.dispatch({type: 'CHANGE_INPUT', value: ct});
            }
        });
    }
    changeDate1() {
        console.log('change data', this.input.value);
    }
}
DatePicker.id = 0;