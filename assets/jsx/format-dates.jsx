class FormatDates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: props.date,
            data: null,
            timeZone: store.getState().timeZ[0],
        };
        moment.locale('ru');
        store.subscribe(( ) => {
            this.setState({data: store.getState().value[0]});
            this.setState({timeZone: store.getState().timeZ[0]})
        })
    }
    relativeTime(data = new Date() ){
        if ( typeof moment.tz  === 'function') {
            return  moment( data ).tz(this.state.timeZone).startOf('day').fromNow()
        }
        return  moment( data ).startOf('day').fromNow()
    }
    formatDatesTime(){
        if ( typeof moment.tz  === 'function') {
            return  moment( new Date()).tz(this.state.timeZone).format('h:mm:ss a');
        }
        return moment(new Date()).format(' h:mm:ss a');
    }
    formatDates(data){
        if ( typeof moment.tz  === 'function') {
            return  moment(data).tz(this.state.timeZone).format('MMMM Do YYYY');
        }
        return moment(data).format('MMMM Do YYYY');
    }
    render() {
        return (
            <div>
                <p> <span className="text">Format Dates:</span> {this.formatDates(this.state.data)} - {  this.formatDatesTime() }</p>
                <p> <span className="text">Relative Time:</span> { this.relativeTime(this.state.data) } </p>
            </div>

        )
    }
}