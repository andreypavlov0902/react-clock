let value = (store = [], action) => {
    if(action.type === 'CHANGE_INPUT') {
        return [ action.value];
    }
    return store;
};
let time = (store = [], action) => {
    return action.type === 'CHANGE_TIME' ? [action.value] : store;
};
let timeZ = (store = ['Europe/Zaporozhye'], action) => {
    return action.type === 'CHANGE_TIME_ZONE' ? [action.value] : store;
};
const combine = Redux.combineReducers({ value, time, timeZ});
const store =  Redux.createStore(combine);

if( typeof moment.tz === 'function') {
    var timeZone =  moment.tz.names();
}
