class TextTime extends React.Component{
    constructor(props){
        super(props);
        this.state = {time: new Date()};
        this.id = null;
        store.subscribe(()=> {
            this.setState({time: store.getState().time[0]})
        })
    }
    get timerId (){
        return this.id;
    }
    set timerId(value) {
        this.id = value;
    }
    componentDidMount() {
        this.tick();
    }
    tick() {
        store.dispatch({type: 'CHANGE_TIME', value: new Date()})
        this.id = setTimeout(() => {
            this.tick();
        }, 1000);
    }
    componentWillUnmount() {
        clearTimeout(this.id);
    }
    stop(){
        clearTimeout(this.id);
    }
    render(){
        return (
            <div>
                <p><span className="text">Now Date not formatted:</span> {this.state.time.toLocaleTimeString()}</p>
                <FormatDates date={this.state.time}/>
            </div>
        )
    }
}