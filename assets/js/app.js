'use strict';

var value = function value(store, action) {
    if (store === undefined) store = [];

    if (action.type === 'CHANGE_INPUT') {
        return [action.value];
    }
    return store;
};
var time = function time(store, action) {
    if (store === undefined) store = [];

    return action.type === 'CHANGE_TIME' ? [action.value] : store;
};
var timeZ = function timeZ(store, action) {
    if (store === undefined) store = ['Europe/Zaporozhye'];

    return action.type === 'CHANGE_TIME_ZONE' ? [action.value] : store;
};
var combine = Redux.combineReducers({ value: value, time: time, timeZ: timeZ });
var store = Redux.createStore(combine);

if (typeof moment.tz === 'function') {
    var timeZone = moment.tz.names();
}
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DatePicker = (function (_React$Component) {
    _inherits(DatePicker, _React$Component);

    function DatePicker(props) {
        _classCallCheck(this, DatePicker);

        _get(Object.getPrototypeOf(DatePicker.prototype), "constructor", this).call(this, props);
        this.changeDate1 = this.changeDate1.bind(this);
        this.id = DatePicker.id++;
    }

    _createClass(DatePicker, [{
        key: "render",
        value: function render() {
            var _this = this;

            return React.createElement(
                "div",
                { className: "form-group custom my-form-group col-md-3" },
                React.createElement(
                    "label",
                    { className: "example-datetime-local-input", htmlFor: 'datetimepicker' + this.id },
                    "Enter Date"
                ),
                React.createElement("input", { className: "form-control md", onChange: this.changeDate1, id: 'datetimepicker' + this.id, type: "text", ref: function (input) {
                        _this.trackInput = input;
                    } })
            );
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            var select = "#datetimepicker" + this.id;
            $(select).datetimepicker({
                onSelectDate: function onSelectDate(ct, $i) {
                    store.dispatch({ type: 'CHANGE_INPUT', value: ct });
                },
                onSelectTime: function onSelectTime(ct, $input) {
                    store.dispatch({ type: 'CHANGE_INPUT', value: ct });
                }
            });
        }
    }, {
        key: "changeDate1",
        value: function changeDate1() {
            console.log('change data', this.input.value);
        }
    }]);

    return DatePicker;
})(React.Component);

DatePicker.id = 0;
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TimeZone = (function (_React$Component) {
    _inherits(TimeZone, _React$Component);

    function TimeZone(props) {
        _classCallCheck(this, TimeZone);

        _get(Object.getPrototypeOf(TimeZone.prototype), 'constructor', this).call(this, props);
        this.change = this.change.bind(this);
    }

    _createClass(TimeZone, [{
        key: 'options',
        value: function options(arr) {
            var res = [];
            arr.forEach(function (element, i) {
                res.push(React.createElement(
                    'option',
                    { key: i, value: element },
                    element
                ));
            });
            return res;
        }
    }, {
        key: 'change',
        value: function change() {
            store.dispatch({ type: 'CHANGE_TIME_ZONE', value: this.select.value });
            console.log('select', this.select.value);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this = this;

            if (typeof moment.tz === 'function') {
                return React.createElement(
                    'div',
                    { className: 'time-zone form-group my-form-group col-md-3' },
                    React.createElement(
                        'label',
                        { className: ' my-label', htmlFor: 'zone' },
                        'Choice the time zone'
                    ),
                    React.createElement(
                        'select',
                        { className: 'custom-select ', onChange: this.change, size: '1', name: 'timeZone', id: 'zone', ref: function (select) {
                                _this.select = select;
                            } },
                        this.options(timeZone)
                    )
                );
            }
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'pre',
                    null,
                    'if you connect moment-timezone.js library instead of moment.js to you will be available time zone choice'
                )
            );
        }
    }]);

    return TimeZone;
})(React.Component);
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x2, _x3, _x4) { var _again = true; _function: while (_again) { var object = _x2, property = _x3, receiver = _x4; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x2 = parent; _x3 = property; _x4 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormatDates = (function (_React$Component) {
    _inherits(FormatDates, _React$Component);

    function FormatDates(props) {
        var _this = this;

        _classCallCheck(this, FormatDates);

        _get(Object.getPrototypeOf(FormatDates.prototype), 'constructor', this).call(this, props);
        this.state = {
            time: props.date,
            data: null,
            timeZone: store.getState().timeZ[0]
        };
        moment.locale('ru');
        store.subscribe(function () {
            _this.setState({ data: store.getState().value[0] });
            _this.setState({ timeZone: store.getState().timeZ[0] });
        });
    }

    _createClass(FormatDates, [{
        key: 'relativeTime',
        value: function relativeTime() {
            var data = arguments.length <= 0 || arguments[0] === undefined ? new Date() : arguments[0];

            if (typeof moment.tz === 'function') {
                return moment(data).tz(this.state.timeZone).startOf('day').fromNow();
            }
            return moment(data).startOf('day').fromNow();
        }
    }, {
        key: 'formatDatesTime',
        value: function formatDatesTime() {
            if (typeof moment.tz === 'function') {
                return moment(new Date()).tz(this.state.timeZone).format('h:mm:ss a');
            }
            return moment(new Date()).format(' h:mm:ss a');
        }
    }, {
        key: 'formatDates',
        value: function formatDates(data) {
            if (typeof moment.tz === 'function') {
                return moment(data).tz(this.state.timeZone).format('MMMM Do YYYY');
            }
            return moment(data).format('MMMM Do YYYY');
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'p',
                    null,
                    ' ',
                    React.createElement(
                        'span',
                        { className: 'text' },
                        'Format Dates:'
                    ),
                    ' ',
                    this.formatDates(this.state.data),
                    ' - ',
                    this.formatDatesTime()
                ),
                React.createElement(
                    'p',
                    null,
                    ' ',
                    React.createElement(
                        'span',
                        { className: 'text' },
                        'Relative Time:'
                    ),
                    ' ',
                    this.relativeTime(this.state.data),
                    ' '
                )
            );
        }
    }]);

    return FormatDates;
})(React.Component);
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextTime = (function (_React$Component) {
    _inherits(TextTime, _React$Component);

    function TextTime(props) {
        var _this = this;

        _classCallCheck(this, TextTime);

        _get(Object.getPrototypeOf(TextTime.prototype), "constructor", this).call(this, props);
        this.state = { time: new Date() };
        this.id = null;
        store.subscribe(function () {
            _this.setState({ time: store.getState().time[0] });
        });
    }

    _createClass(TextTime, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            this.tick();
        }
    }, {
        key: "tick",
        value: function tick() {
            var _this2 = this;

            store.dispatch({ type: 'CHANGE_TIME', value: new Date() });
            this.id = setTimeout(function () {
                _this2.tick();
            }, 1000);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            clearTimeout(this.id);
        }
    }, {
        key: "stop",
        value: function stop() {
            clearTimeout(this.id);
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(
                    "p",
                    null,
                    React.createElement(
                        "span",
                        { className: "text" },
                        "Now Date not formatted:"
                    ),
                    " ",
                    this.state.time.toLocaleTimeString()
                ),
                React.createElement(FormatDates, { date: this.state.time })
            );
        }
    }, {
        key: "timerId",
        get: function get() {
            return this.id;
        },
        set: function set(value) {
            this.id = value;
        }
    }]);

    return TextTime;
})(React.Component);
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Widget = (function (_React$Component) {
    _inherits(Widget, _React$Component);

    function Widget(props) {
        _classCallCheck(this, Widget);

        _get(Object.getPrototypeOf(Widget.prototype), "constructor", this).call(this, props);
    }

    _createClass(Widget, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(TimeZone, null),
                React.createElement(DatePicker, null),
                React.createElement(TextTime, null)
            );
        }
    }]);

    return Widget;
})(React.Component);
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MyApp = (function (_React$Component) {
    _inherits(MyApp, _React$Component);

    function MyApp(props) {
        _classCallCheck(this, MyApp);

        _get(Object.getPrototypeOf(MyApp.prototype), 'constructor', this).call(this, props);
        this.state = { value: '' };
    }

    _createClass(MyApp, [{
        key: 'render',
        value: function render() {
            return React.createElement(Widget, null);
        }
    }]);

    return MyApp;
})(React.Component);

ReactDOM.render(React.createElement(MyApp, { state: store.getState() }), document.getElementById('app'));
//# sourceMappingURL=app.js.map
